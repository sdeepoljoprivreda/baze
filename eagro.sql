-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: eagro
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `associationmembers`
--

DROP TABLE IF EXISTS `associationmembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `associationmembers` (
  `associationId` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `calendarevents` tinyint(1) DEFAULT '1',
  `notifications` tinyint(1) DEFAULT '1',
  `assoicationrole` varchar(30) DEFAULT 'MEMBER',
  PRIMARY KEY (`username`,`associationId`),
  KEY `associationId` (`associationId`),
  CONSTRAINT `associationmembers_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `associationmembers_ibfk_2` FOREIGN KEY (`associationId`) REFERENCES `associations` (`associationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `associationmembers`
--

LOCK TABLES `associationmembers` WRITE;
/*!40000 ALTER TABLE `associationmembers` DISABLE KEYS */;
/*!40000 ALTER TABLE `associationmembers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `associationposts`
--

DROP TABLE IF EXISTS `associationposts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `associationposts` (
  `associationId` int(11) NOT NULL,
  `postId` int(11) NOT NULL,
  PRIMARY KEY (`postId`,`associationId`),
  KEY `associationId` (`associationId`),
  CONSTRAINT `associationposts_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `posts` (`postId`),
  CONSTRAINT `associationposts_ibfk_2` FOREIGN KEY (`associationId`) REFERENCES `associations` (`associationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `associationposts`
--

LOCK TABLES `associationposts` WRITE;
/*!40000 ALTER TABLE `associationposts` DISABLE KEYS */;
/*!40000 ALTER TABLE `associationposts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `associations`
--

DROP TABLE IF EXISTS `associations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `associations` (
  `associationId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `landarea` float NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `foundingdate` date NOT NULL,
  `website` varchar(45) DEFAULT NULL,
  `headquarters` varchar(45) NOT NULL,
  `field` varchar(45) NOT NULL,
  PRIMARY KEY (`associationId`),
  UNIQUE KEY `id_UNIQUE` (`associationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `associations`
--

LOCK TABLES `associations` WRITE;
/*!40000 ALTER TABLE `associations` DISABLE KEYS */;
/*!40000 ALTER TABLE `associations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificates`
--

DROP TABLE IF EXISTS `certificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `certificates` (
  `certificateId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`certificateId`),
  UNIQUE KEY `id_UNIQUE` (`certificateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificates`
--

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversation`
--

DROP TABLE IF EXISTS `conversation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `conversation` (
  `conversationid` int(11) NOT NULL AUTO_INCREMENT,
  `user1` varchar(45) NOT NULL,
  `user2` varchar(45) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`conversationid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversation`
--

LOCK TABLES `conversation` WRITE;
/*!40000 ALTER TABLE `conversation` DISABLE KEYS */;
INSERT INTO `conversation` VALUES (1,'miodrag','dusan','2019-05-13 02:00:00');
/*!40000 ALTER TABLE `conversation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipments`
--

DROP TABLE IF EXISTS `equipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `equipments` (
  `equipmentId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `productionDate` date NOT NULL,
  `registrationNumber` varchar(45) NOT NULL,
  `brand` varchar(45) NOT NULL,
  `countryOfOrigin` varchar(45) NOT NULL,
  PRIMARY KEY (`equipmentId`),
  UNIQUE KEY `id_UNIQUE` (`equipmentId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipments`
--

LOCK TABLES `equipments` WRITE;
/*!40000 ALTER TABLE `equipments` DISABLE KEYS */;
INSERT INTO `equipments` VALUES (2,'miodrag','da','1982-04-30','12321','sda','nepal'),(3,'miodrag','da','1982-04-30','12321','sda','nepal'),(4,'miodrag','da','1982-04-30','12321','sda','nepal'),(5,'miodrag','da','1982-04-30','12321','sda','nepal'),(6,'miodrag','da','1982-04-30','12321','sda','nepal'),(7,'miodra','da','1982-04-30','12321','sda','nepal'),(8,'miodra','da','1982-04-30','11232','sda','nepal'),(9,'miodra','da','1982-04-30','11232','sda','nepal');
/*!40000 ALTER TABLE `equipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupassociations`
--

DROP TABLE IF EXISTS `groupassociations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `groupassociations` (
  `groupId` int(11) NOT NULL,
  `associationId` int(11) NOT NULL,
  PRIMARY KEY (`groupId`,`associationId`),
  KEY `associationId` (`associationId`),
  CONSTRAINT `groupassociations_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `grups` (`groupId`),
  CONSTRAINT `groupassociations_ibfk_2` FOREIGN KEY (`associationId`) REFERENCES `associations` (`associationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupassociations`
--

LOCK TABLES `groupassociations` WRITE;
/*!40000 ALTER TABLE `groupassociations` DISABLE KEYS */;
/*!40000 ALTER TABLE `groupassociations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupmembers`
--

DROP TABLE IF EXISTS `groupmembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `groupmembers` (
  `groupId` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `grouprole` varchar(30) DEFAULT 'MEMBER',
  `notifications` tinyint(1) DEFAULT '0',
  `calendarevents` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`username`,`groupId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `groupmembers_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `groupmembers_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `grups` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupmembers`
--

LOCK TABLES `groupmembers` WRITE;
/*!40000 ALTER TABLE `groupmembers` DISABLE KEYS */;
INSERT INTO `groupmembers` VALUES (1,'bojan','MEMBER',0,0),(1,'dusan','MEMBER',0,0),(2,'dusan','MEMBER',0,0),(7,'jelena','MEMBER',0,0),(3,'miodrag','MEMBER',0,0),(8,'miodrag','MEMBER',0,0),(1,'pavle','MEMBER',0,0),(4,'pavle','MEMBER',0,0),(2,'petar','MEMBER',0,0);
/*!40000 ALTER TABLE `groupmembers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupposts`
--

DROP TABLE IF EXISTS `groupposts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `groupposts` (
  `groupId` int(11) NOT NULL,
  `postId` int(11) NOT NULL,
  PRIMARY KEY (`postId`,`groupId`),
  KEY `groupId` (`groupId`),
  CONSTRAINT `groupposts_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `posts` (`postId`),
  CONSTRAINT `groupposts_ibfk_2` FOREIGN KEY (`groupId`) REFERENCES `grups` (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupposts`
--

LOCK TABLES `groupposts` WRITE;
/*!40000 ALTER TABLE `groupposts` DISABLE KEYS */;
INSERT INTO `groupposts` VALUES (1,1),(1,3),(1,4),(2,5),(2,9),(3,3),(4,10),(4,11),(7,6),(10,1);
/*!40000 ALTER TABLE `groupposts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grups`
--

DROP TABLE IF EXISTS `grups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `grups` (
  `groupId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `owner` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`groupId`),
  UNIQUE KEY `id_UNIQUE` (`groupId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grups`
--

LOCK TABLES `grups` WRITE;
/*!40000 ALTER TABLE `grups` DISABLE KEYS */;
INSERT INTO `grups` VALUES (1,'pavle','64','miodrag'),(2,'osa','64','miodrag'),(3,'zebra','64','miodrag'),(4,'pcelari','64','miodrag'),(5,'losw','64','miodrag'),(6,'warz','64','miodrag'),(7,'trem','64','miodrag'),(8,'busk','1','miodrag'),(9,'zar','t','miodrag'),(10,'rez','z','miodrag'),(12,'sadad','z','miodrag');
/*!40000 ALTER TABLE `grups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `images` (
  `imageid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `ppath` varchar(255) DEFAULT NULL,
  `backgroundpath` varchar(255) DEFAULT NULL,
  `profilepath` varchar(255) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  `associationid` int(11) DEFAULT NULL,
  `productid` int(11) DEFAULT NULL,
  PRIMARY KEY (`imageid`),
  UNIQUE KEY `id_UNIQUE` (`imageid`),
  KEY `username` (`username`),
  KEY `groupid` (`groupid`),
  KEY `productid` (`productid`),
  KEY `associationid` (`associationid`),
  CONSTRAINT `images_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `images_ibfk_2` FOREIGN KEY (`groupid`) REFERENCES `grups` (`groupId`),
  CONSTRAINT `images_ibfk_3` FOREIGN KEY (`productid`) REFERENCES `products` (`productId`),
  CONSTRAINT `images_ibfk_4` FOREIGN KEY (`associationid`) REFERENCES `associations` (`associationId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` VALUES (1,'dusan',NULL,'C:/Users/Andrej/Desktop/EagroPictures/Users/dusan/Background/CheckPoint_1212b2_7e41a000_56b2cb1.png','C:/Users/Andrej/Desktop/EagroPictures/Users/dusan/Profile/AutoSave_1212b2_7e41a000_5779cd0.png',NULL,NULL,NULL);
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landowners`
--

DROP TABLE IF EXISTS `landowners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `landowners` (
  `landId` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  PRIMARY KEY (`username`,`landId`),
  KEY `landId` (`landId`),
  CONSTRAINT `landowners_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `landowners_ibfk_2` FOREIGN KEY (`landId`) REFERENCES `lands` (`landId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landowners`
--

LOCK TABLES `landowners` WRITE;
/*!40000 ALTER TABLE `landowners` DISABLE KEYS */;
/*!40000 ALTER TABLE `landowners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landproductions`
--

DROP TABLE IF EXISTS `landproductions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `landproductions` (
  `landId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `areaOfProduction` float NOT NULL,
  `averageProduction` float NOT NULL,
  PRIMARY KEY (`landId`,`productId`),
  KEY `productId` (`productId`),
  CONSTRAINT `landproductions_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`),
  CONSTRAINT `landproductions_ibfk_2` FOREIGN KEY (`landId`) REFERENCES `lands` (`landId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landproductions`
--

LOCK TABLES `landproductions` WRITE;
/*!40000 ALTER TABLE `landproductions` DISABLE KEYS */;
/*!40000 ALTER TABLE `landproductions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lands`
--

DROP TABLE IF EXISTS `lands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lands` (
  `landId` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(30) NOT NULL,
  `area` float NOT NULL,
  PRIMARY KEY (`landId`),
  UNIQUE KEY `id_UNIQUE` (`landId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lands`
--

LOCK TABLES `lands` WRITE;
/*!40000 ALTER TABLE `lands` DISABLE KEYS */;
INSERT INTO `lands` VALUES (1,'pavle',64),(2,'pavle',64),(3,'pavle',64);
/*!40000 ALTER TABLE `lands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ownedcertificates`
--

DROP TABLE IF EXISTS `ownedcertificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ownedcertificates` (
  `certificateId` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `dateOfAcquirement` date NOT NULL,
  `locationOfAcquirement` varchar(45) NOT NULL,
  PRIMARY KEY (`username`,`certificateId`),
  KEY `certificateId` (`certificateId`),
  CONSTRAINT `ownedcertificates_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `ownedcertificates_ibfk_2` FOREIGN KEY (`certificateId`) REFERENCES `certificates` (`certificateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ownedcertificates`
--

LOCK TABLES `ownedcertificates` WRITE;
/*!40000 ALTER TABLE `ownedcertificates` DISABLE KEYS */;
/*!40000 ALTER TABLE `ownedcertificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ownedequipments`
--

DROP TABLE IF EXISTS `ownedequipments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ownedequipments` (
  `equipmentId` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`username`,`equipmentId`),
  KEY `equipmentId` (`equipmentId`),
  CONSTRAINT `ownedequipments_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `ownedequipments_ibfk_2` FOREIGN KEY (`equipmentId`) REFERENCES `equipments` (`equipmentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ownedequipments`
--

LOCK TABLES `ownedequipments` WRITE;
/*!40000 ALTER TABLE `ownedequipments` DISABLE KEYS */;
/*!40000 ALTER TABLE `ownedequipments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `posts` (
  `postId` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `username` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `text` varchar(500) NOT NULL,
  `parentid` int(11) DEFAULT '0',
  PRIMARY KEY (`postId`),
  UNIQUE KEY `id_UNIQUE` (`postId`),
  KEY `username` (`username`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Test','miodrag','2019-05-13','Ovo je test',0),(2,'Pop','miodrag','2019-05-13','Ovo je test',0),(3,'Da','miodrag','2019-05-13','Ovo je test',0),(4,'lop','pavle','2019-05-13','Novi',0),(5,'lop','dusan','2019-05-13','Novi',0),(6,'lop','vuk','2019-05-13','Novi',0),(7,'lop','milos','2019-05-13','Novi',0),(8,'lop','marko','2019-05-13','Novi',0),(9,'lop','vuk','2019-05-13','Novi',0),(10,'lop','jelena','2019-05-13','Novi',0),(11,'lop','jelena','2019-05-13','Novi',0),(12,'lop','jelena','2019-05-13','Novi',0),(13,'lop','miodrag','2019-05-13','Novi',0),(14,'lop','miodrag','2019-05-13','Novi',0);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privatemessage`
--

DROP TABLE IF EXISTS `privatemessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `privatemessage` (
  `privatemessageid` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(45) NOT NULL,
  `conversationid` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `text` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`privatemessageid`),
  KEY `conversationid` (`conversationid`),
  CONSTRAINT `privatemessage_ibfk_1` FOREIGN KEY (`conversationid`) REFERENCES `conversation` (`conversationid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privatemessage`
--

LOCK TABLES `privatemessage` WRITE;
/*!40000 ALTER TABLE `privatemessage` DISABLE KEYS */;
INSERT INTO `privatemessage` VALUES (1,'miodrag',1,'2019-05-13 02:00:00','Novi'),(2,'miodrag',1,'2019-05-13 02:00:00',''),(3,'miodrag',1,'2019-05-13 02:00:00',NULL);
/*!40000 ALTER TABLE `privatemessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productcertificates`
--

DROP TABLE IF EXISTS `productcertificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `productcertificates` (
  `productId` int(11) NOT NULL,
  `certificateId` int(11) NOT NULL,
  PRIMARY KEY (`productId`,`certificateId`),
  KEY `certificateId` (`certificateId`),
  CONSTRAINT `productcertificates_ibfk_1` FOREIGN KEY (`certificateId`) REFERENCES `certificates` (`certificateId`),
  CONSTRAINT `productcertificates_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productcertificates`
--

LOCK TABLES `productcertificates` WRITE;
/*!40000 ALTER TABLE `productcertificates` DISABLE KEYS */;
/*!40000 ALTER TABLE `productcertificates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `products` (
  `productId` int(11) NOT NULL AUTO_INCREMENT,
  `productName` varchar(45) NOT NULL,
  `productType` varchar(45) NOT NULL,
  `fertilizer` varchar(45) NOT NULL,
  PRIMARY KEY (`productId`),
  UNIQUE KEY `id_UNIQUE` (`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refreshtoken`
--

DROP TABLE IF EXISTS `refreshtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `refreshtoken` (
  `value` varchar(300) NOT NULL,
  `username` varchar(45) NOT NULL,
  `expirationdate` datetime DEFAULT NULL,
  PRIMARY KEY (`value`),
  KEY `username` (`username`),
  CONSTRAINT `refreshtoken_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refreshtoken`
--

LOCK TABLES `refreshtoken` WRITE;
/*!40000 ALTER TABLE `refreshtoken` DISABLE KEYS */;
INSERT INTO `refreshtoken` VALUES ('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJib2phbiIsImV4cCI6MTU4NzY0MDExN30.ZSdDXPhedrSEnHZF8xTKVzcm836KhlJXTAlnfETjYNqUjqi-9LwGa1wWmxPZohvRtllDLCZL41ukvHimoGeI2g','bojan','2020-04-23 13:08:38'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkdXNhbiIsImV4cCI6MTU4NzgzMDA2MX0.XnAq64Xdn2YteAPnl6-y9I--xPpa_c52RL9I6PcZnsULLGBPz8azCA-UMbiTmA2qGpa7nZBIsPFhEfnxQtAumA','dusan','2020-04-25 17:54:21'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkdXNhbiIsImV4cCI6MTU4NzMyMTU4MH0.4TYBeMTosVlWMtL6exMNRv3zVG7Xh_U7gGELlYKkOHT3SlUd2rP_dA-sZCP43tF8zKN878K48XbTSgtamZuVRQ','dusan','2020-04-19 00:08:16'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkdXNhbiIsImV4cCI6MTU4NzYzNjYyMH0.bY1y9anKCflv82F3HNoyfiaQex1idDcgAl3iHLL6A0ODnb8XfJdg54agjYmMucu8djmGGr02Am0lAAP666tFJw','dusan','2020-04-23 12:10:20'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkdXNhbiIsImV4cCI6MTU4NzYzOTQxNX0.EeKYWeqURz8FmJoj5uIvXe-M11omrqKHfdLAhhpUTfE0iB6TU_mdva5i3A3ZhcEEjuAZXoEEbWsGwiTXOfWqTw','dusan','2020-04-23 12:56:56'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkdXNhbiIsImV4cCI6MTU4OTczMDczNn0.NdGQzdZIlWAUjzqI8xZaObS2Jk57y_x-x24HsHm6_e5GgXc7KZC0fmRhqf-UpvADaCjD5FzVEV8vnF5YRUT5Zw','dusan','2020-05-17 17:52:16'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkdXNhbiIsImV4cCI6MTU4OTczNDQ4OX0.b8BmDdnVCEByad8wzfbgzFSqAtEuKTTofqBHXRN1XmvaNUxfJm9n3n7IngTNM8Yw1MqrUG9ontXRF8lurq7TCg','dusan','2020-05-17 18:54:50'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsYWxhbyIsImV4cCI6MTU4NzY0MDEyNn0.-sQPsv-mQx_9kk8-A2DN7Cr02-czBLIf8Q3t2GIBVeMShvoREG8i9yqUX5qnazhwpR3kH_k4ShLWO5Wxx3UZww','lalao','2020-04-23 13:08:46'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaW9kcmFnIiwiZXhwIjoxNTg3NjQwMjAwfQ.uewJmmTYul89G7XtHdsjGI8HSiFg63RMuKGLagd3xTYCW7js4Cwc_bdEb2Ae3-rxlP4vNFX1rhSPzRDyqgLQ2w','miodrag','2020-04-23 13:10:00'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4NzY0MDE4OX0.lLeNU8DGflyoHhu2gb5ClpbOU60wg2SGuL3go_MpTGH6sUP_MZjtLJGr0a-Yb5eOE6ykF7dOLdI7J9O_D-EtuQ','pavle','2020-04-23 13:09:50'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYxNzE2Nn0.9AN1TQz-Ar_FEn1rR9ZXw59pQD80pNytvUwiEef-HBXIkwm7Y7UF_4vcbLBZxetRByqbJZVcteWxO5YO2yiVmA','pavle','2020-05-04 20:32:46'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYxNzI1MX0.9BwdRU6jc3GVQx5Yen9nEWOuQiIeAS6hWFr3nXwixPDosCdxqJPvI1TsjOvZZP1OvtYZHgQKs5QNDyJZVyK-oA','pavle','2020-05-04 20:34:12'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYxNzIwNX0.7T2MQPPFpVLiGcxyJrwqngYBKOTWfUvPM8b7XLv9dlPZlqmYrsKrrnTTmEsrBvxc_-NLFzr6w1QQFn5iozCIEQ','pavle','2020-05-04 20:33:25'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYxNzM1NX0.qSI_3cmCPZKBA5NAwtiIGOAS6tyGrCWnjZ4Sp2vswuFsPY4Y6mlDeBMWAEMaEVXbVz4AZf2myk9r1krD23L7LA','pavle','2020-05-04 20:35:55'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYxNzQwOX0.TkXWZ2sUd6wOWfdXp3GlZtsyZLBJ8xnOR0fPwtIKAy-rHYbuxapkjdFB27dLG34jJEHYlWjqEEmZzqjJwa65rg','pavle','2020-05-04 20:36:50'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYxOTQwN30.jyAZmLZ1v7N8FVEuzXWY4mKuOzXfwD8CGwgBuoCx1j-NFHKkfjpHJ3koSIOg4iAxzJPgiYRTjHWrbh_6kmH0mg','pavle','2020-05-04 21:10:08'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYxOTU1NX0.IAHOhG0VeaNDUukovJGFkOld38g90nMIs2HP3TWOxrlHthpkTBlq6knrFcbgSg32JloCurTQm15jbT_w4oom_g','pavle','2020-05-04 21:12:35'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYyMDc2MH0.TibEXfJheQKXJeX-rzAuId0Bj6es-K9fQgV-MEuTzJ8Q97qfmctnX_xXSIuqJxOtmUrSg5MGc7aKdNSMwypsyg','pavle','2020-05-04 21:32:40'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYyMTA5NH0.wFgNqP88603Qsc2APTx3Os5IN4Y429kG9Nx0S1dKT9Fcj3NNrxYWGc1oiYU2C8GgSMm-mFxYvBK2kuR2b9CY6Q','pavle','2020-05-04 21:38:15'),('eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJwYXZsZSIsImV4cCI6MTU4ODYyMTAyMn0.4w6PnHCanOUkBUMIRVqeAUcDO7ofgxRNRRBSCZtjBp2nxLnMxL8WhmBO_YjiB473v88FpGOuA3Uweo41GN93ew','pavle','2020-05-04 21:37:03');
/*!40000 ALTER TABLE `refreshtoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roles` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`roleid`),
  UNIQUE KEY `id_UNIQUE` (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'USER'),(2,'ADMIN');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `username` varchar(30) NOT NULL,
  `password` varchar(150) DEFAULT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `birthdate` date NOT NULL,
  `pib` varchar(30) DEFAULT NULL,
  `phonenumber` varchar(45) NOT NULL,
  `website` varchar(45) DEFAULT NULL,
  `address` varchar(45) NOT NULL,
  `profession` varchar(45) NOT NULL,
  `sendnews` tinyint(1) DEFAULT NULL,
  `socialmedia` varchar(45) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`),
  UNIQUE KEY `id_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('bojan','$2a$10$FU3J3/AzEJFxcPiQWRwidursA49cLkGSgKRvyRpzpkztsEFKG3s8.','bojan','zoric','bojan@gmail.com','1982-04-29','1504987513225','0643217844','www.wrbsajt.rs','jagoda','pcelar',1,'',1),('dusan','$2a$10$r94fnkBFB58m4WcfhrXj2O3ISOpS0QDOYHYzsDjvFLgZIc2Esf9Xu','dusan','lazic','dusan@gmail.com','1982-04-30','1504987513221','0645297844','www.veb2ajt.rs','zivka davidovica 33','pcelar',0,'',1),('jelena','$2a$10$zVf/31um0M8mDZosmxaIleomscZGyFjy0Fer7.4FkoB9XTFjaVn8e','jelena','rakic','jelena@gmail.com','1989-07-20','1504987513222','0646797844','www.rebsajt.rs','prizrenska 5','pcelar',0,'',1),('lalao','$2a$10$mr04.LsGOgRj7ZAumQzaNuwrywA6QF8hoc1.QrtSdKownN4ySGNui','bojan','zoric','bojans@gmail.com','1982-04-30','1504987513225','0643217844','www.wrbsajt.rs','jagoda','pcelar',0,'',1),('lo','$2a$10$ylXyz3sDD67qQmnhWZHuv.oedIKu9xGyIg2w/Sy2nEKls8wMUSQTO','bojan','zoric','bojans@gmail.com','1982-04-30','1504987513225','0643217844','www.wrbsajt.rs','jagoda','pcelar',0,'',1),('marina','$2a$10$1enE3q4hHNlzYk/zaeXBjeNJoH4ONhf6DehjEbx0wvZ9GwHvRCFG6','marina','vacic','marina@gmail.com','1982-04-30','1504987513226','0647297844','www.vepsajt.rs','kruska','pcelar',0,'',1),('marko','$2a$10$UsEo5kaIr4R8MUOtGxHspO3dHtQ3EZh23CrzgHxbsyzayY.YAJQ6q','marko','ruzic','marko@gmail.com','1983-07-30','1504987513223','0643299244','www.veblajt.rs','pozeska 12','pcelar',0,'',1),('milo','$2a$10$kB4x9K2omLpCKrs1j/Aw5ulZaEhjG/leVP1Nq8hjCoavUdn3/a8.2','bojan','zoric','bojans@gmail.com','1982-04-30','1504987513225','0643217844','www.wrbsajt.rs','jagoda','pcelar',0,'',1),('milos','$2a$10$K2Vu7SFzxpBqRzdq7yDOG.neka.t0Uhz3JX1OUwJ1.tUjEWYdy2em','milos','ristic','milos@gmail.com','1982-04-30','1504987513227','0644297844','www.veb2ajt.rs','kraljice natalije 22','pcelar',0,'',1),('miodrag','$2a$10$CiyGKbaRF3Xwh5e6GNUz5eAfNk.Z4rrJSHE6jiIoIvZpFzRw9NlYK','Miodrag','Zivkovic','mzivkovic@singidunum.ac.rs','1982-04-30','1011991122554','111222333','ww.abc.com','Beograd','profesor',1,'Facebook',1),('mirko','$2a$10$y3CIk0f99pk3Sqn/rJ/T0eq8.gIGadPCFEfwuy0/NoBrdDJwqOa0u','bojan','zoric','bojans@gmail.com','1982-04-30','1504987513225','0643217844','www.wrbsajt.rs','jagoda','pcelar',0,'',1),('mlo','$2a$10$0KB4I3Cz9GNwuShOu8pDTOD.L/OI9m8quvGuQJFLT.B0pb/GFgh8u','bojan','zoric','bojans@gmail.com','1982-04-30','1504987513225','0643217844','www.wrbsajt.rs','jagoda','pcelar',0,'',1),('pavle','$2a$10$WdizfjJDhyAjFY38lnaEoOmSCKP7xpjTYlAiGWxR6LBdrFlRWw1Zy','pavle','jovic','pavlejovic@gmail.com','1982-04-30','1504987513224','0643297154','www.vebsajt.rs','golsvordijeva 33','pcelar',0,'',1),('petar','$2a$10$TstH8goQin4x/RHxUNBZ8.EwO.Iq1Cp95pV2MPGxhwzpIbyEWbfUW','petar','jovic','petar@gmail.com','1982-04-30','1504987513228','0643397844','www.vebs1ajt.rs','ruzveltova 21','pcelar',0,'',1),('sdaospad','$2a$10$TgNr6meZwGdHB1szaCjZzuKE0/xqPdC0OvGFOjSCBljgbjICYBzHG','jelena','rakic','jeleasdadana@gmail.com','1989-07-20','3504987513222','0646797844','www.rebsajt.rs','prizrenska 5','pcelar',0,'',1),('vuk','$2a$10$xCJiOb5ONTY4SzJ4AnPEbeqvCuamQRcMG29BXDY.5S83WbLURsmFa','vuk','micic','vuk@gmail.com','1982-04-30','1504987513220','0643297843','www.vebsaja.rs','marka oraskovica 5','pcelar',0,'',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usersroles`
--

DROP TABLE IF EXISTS `usersroles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `usersroles` (
  `username` varchar(30) NOT NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY (`username`,`roleid`),
  KEY `roleid` (`roleid`),
  CONSTRAINT `usersroles_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`),
  CONSTRAINT `usersroles_ibfk_2` FOREIGN KEY (`roleid`) REFERENCES `roles` (`roleid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersroles`
--

LOCK TABLES `usersroles` WRITE;
/*!40000 ALTER TABLE `usersroles` DISABLE KEYS */;
/*!40000 ALTER TABLE `usersroles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-24 20:09:08
